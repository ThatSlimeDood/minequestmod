
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.minequest.init;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.RegistryEvent;

import net.minecraft.world.level.block.Block;

import net.mcreator.minequest.block.ZirconiumOreBlock;
import net.mcreator.minequest.block.ZincOreBlock;
import net.mcreator.minequest.block.YttriumOreBlock;
import net.mcreator.minequest.block.VanadiumOreBlock;
import net.mcreator.minequest.block.UraniumOreBlock;
import net.mcreator.minequest.block.TungstenOreBlock;
import net.mcreator.minequest.block.TitaniumOreBlock;
import net.mcreator.minequest.block.TantalumOreBlock;
import net.mcreator.minequest.block.SilverOreBlock;
import net.mcreator.minequest.block.RadiumOreBlock;
import net.mcreator.minequest.block.PlatinumOreBlock;
import net.mcreator.minequest.block.PalladiumOreBlock;
import net.mcreator.minequest.block.NickelOreBlock;
import net.mcreator.minequest.block.MercuryBlock;
import net.mcreator.minequest.block.ManganeseOreBlock;
import net.mcreator.minequest.block.LithiumOreBlock;
import net.mcreator.minequest.block.IridiumOreBlock;
import net.mcreator.minequest.block.HafniumOreBlock;
import net.mcreator.minequest.block.CobaltOreBlock;
import net.mcreator.minequest.block.CadmiumOreBlock;

import java.util.List;
import java.util.ArrayList;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class MinequestModBlocks {
	private static final List<Block> REGISTRY = new ArrayList<>();
	public static final Block TITANIUM_ORE = register(new TitaniumOreBlock());
	public static final Block COBALT_ORE = register(new CobaltOreBlock());
	public static final Block PLATINUM_ORE = register(new PlatinumOreBlock());
	public static final Block URANIUM_ORE = register(new UraniumOreBlock());
	public static final Block TUNGSTEN_ORE = register(new TungstenOreBlock());
	public static final Block LITHIUM_ORE = register(new LithiumOreBlock());
	public static final Block RADIUM_ORE = register(new RadiumOreBlock());
	public static final Block NICKEL_ORE = register(new NickelOreBlock());
	public static final Block ZINC_ORE = register(new ZincOreBlock());
	public static final Block MANGANESE_ORE = register(new ManganeseOreBlock());
	public static final Block VANADIUM_ORE = register(new VanadiumOreBlock());
	public static final Block SILVER_ORE = register(new SilverOreBlock());
	public static final Block CADMIUM_ORE = register(new CadmiumOreBlock());
	public static final Block IRIDIUM_ORE = register(new IridiumOreBlock());
	public static final Block HAFNIUM_ORE = register(new HafniumOreBlock());
	public static final Block TANTALUM_ORE = register(new TantalumOreBlock());
	public static final Block ZIRCONIUM_ORE = register(new ZirconiumOreBlock());
	public static final Block PALLADIUM_ORE = register(new PalladiumOreBlock());
	public static final Block YTTRIUM_ORE = register(new YttriumOreBlock());
	public static final Block MERCURY = register(new MercuryBlock());

	private static Block register(Block block) {
		REGISTRY.add(block);
		return block;
	}

	@SubscribeEvent
	public static void registerBlocks(RegistryEvent.Register<Block> event) {
		event.getRegistry().registerAll(REGISTRY.toArray(new Block[0]));
	}
}
