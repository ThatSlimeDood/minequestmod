
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.minequest.init;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.RegistryEvent;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.BlockItem;

import net.mcreator.minequest.item.ZirconiumIngotItem;
import net.mcreator.minequest.item.ZincIngotItem;
import net.mcreator.minequest.item.YttriumIngotItem;
import net.mcreator.minequest.item.VanadiumIngotItem;
import net.mcreator.minequest.item.UraniumIngotItem;
import net.mcreator.minequest.item.TungstenIngotItem;
import net.mcreator.minequest.item.TitaniumIngotItem;
import net.mcreator.minequest.item.TantalumIngotItem;
import net.mcreator.minequest.item.SilverIngotItem;
import net.mcreator.minequest.item.RadiumIngotItem;
import net.mcreator.minequest.item.PlatinumIngotItem;
import net.mcreator.minequest.item.PalladiumIngotItem;
import net.mcreator.minequest.item.NickelIngotItem;
import net.mcreator.minequest.item.MercuryItem;
import net.mcreator.minequest.item.ManganeseIngotItem;
import net.mcreator.minequest.item.LithiumIngotItem;
import net.mcreator.minequest.item.IridiumIngotItem;
import net.mcreator.minequest.item.HafniumIngotItem;
import net.mcreator.minequest.item.CobaltIngotItem;
import net.mcreator.minequest.item.CadmiumIngotItem;

import java.util.List;
import java.util.ArrayList;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class MinequestModItems {
	private static final List<Item> REGISTRY = new ArrayList<>();
	public static final Item TITANIUM_ORE = register(MinequestModBlocks.TITANIUM_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item COBALT_ORE = register(MinequestModBlocks.COBALT_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item TITANIUM_INGOT = register(new TitaniumIngotItem());
	public static final Item COBALT_INGOT = register(new CobaltIngotItem());
	public static final Item PLATINUM_ORE = register(MinequestModBlocks.PLATINUM_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item PLATINUM_INGOT = register(new PlatinumIngotItem());
	public static final Item URANIUM_ORE = register(MinequestModBlocks.URANIUM_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item URANIUM_INGOT = register(new UraniumIngotItem());
	public static final Item TUNGSTEN_ORE = register(MinequestModBlocks.TUNGSTEN_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item TUNGSTEN_INGOT = register(new TungstenIngotItem());
	public static final Item LITHIUM_ORE = register(MinequestModBlocks.LITHIUM_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item LITHIUM_INGOT = register(new LithiumIngotItem());
	public static final Item RADIUM_ORE = register(MinequestModBlocks.RADIUM_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item RADIUM_INGOT = register(new RadiumIngotItem());
	public static final Item NICKEL_ORE = register(MinequestModBlocks.NICKEL_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item NICKEL_INGOT = register(new NickelIngotItem());
	public static final Item ZINC_ORE = register(MinequestModBlocks.ZINC_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item ZINC_INGOT = register(new ZincIngotItem());
	public static final Item MANGANESE_ORE = register(MinequestModBlocks.MANGANESE_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item MANGANESE_INGOT = register(new ManganeseIngotItem());
	public static final Item VANADIUM_ORE = register(MinequestModBlocks.VANADIUM_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item VANADIUM_INGOT = register(new VanadiumIngotItem());
	public static final Item SILVER_ORE = register(MinequestModBlocks.SILVER_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item SILVER_INGOT = register(new SilverIngotItem());
	public static final Item CADMIUM_ORE = register(MinequestModBlocks.CADMIUM_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item CADMIUM_INGOT = register(new CadmiumIngotItem());
	public static final Item IRIDIUM_ORE = register(MinequestModBlocks.IRIDIUM_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item IRIDIUM_INGOT = register(new IridiumIngotItem());
	public static final Item HAFNIUM_ORE = register(MinequestModBlocks.HAFNIUM_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item HAFNIUM_INGOT = register(new HafniumIngotItem());
	public static final Item TANTALUM_ORE = register(MinequestModBlocks.TANTALUM_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item TANTALUM_INGOT = register(new TantalumIngotItem());
	public static final Item ZIRCONIUM_ORE = register(MinequestModBlocks.ZIRCONIUM_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item PALLADIUM_ORE = register(MinequestModBlocks.PALLADIUM_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item YTTRIUM_ORE = register(MinequestModBlocks.YTTRIUM_ORE, CreativeModeTab.TAB_BUILDING_BLOCKS);
	public static final Item ZIRCONIUM_INGOT = register(new ZirconiumIngotItem());
	public static final Item PALLADIUM_INGOT = register(new PalladiumIngotItem());
	public static final Item YTTRIUM_INGOT = register(new YttriumIngotItem());
	public static final Item MERCURY_BUCKET = register(new MercuryItem());

	private static Item register(Item item) {
		REGISTRY.add(item);
		return item;
	}

	private static Item register(Block block, CreativeModeTab tab) {
		return register(new BlockItem(block, new Item.Properties().tab(tab)).setRegistryName(block.getRegistryName()));
	}

	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> event) {
		event.getRegistry().registerAll(REGISTRY.toArray(new Item[0]));
	}
}
