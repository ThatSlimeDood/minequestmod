
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.minequest.init;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.event.RegistryEvent;

import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.data.BuiltinRegistries;
import net.minecraft.core.Registry;

import net.mcreator.minequest.world.features.ores.ZirconiumOreFeature;
import net.mcreator.minequest.world.features.ores.ZincOreFeature;
import net.mcreator.minequest.world.features.ores.YttriumOreFeature;
import net.mcreator.minequest.world.features.ores.VanadiumOreFeature;
import net.mcreator.minequest.world.features.ores.UraniumOreFeature;
import net.mcreator.minequest.world.features.ores.TungstenOreFeature;
import net.mcreator.minequest.world.features.ores.TitaniumOreFeature;
import net.mcreator.minequest.world.features.ores.TantalumOreFeature;
import net.mcreator.minequest.world.features.ores.SilverOreFeature;
import net.mcreator.minequest.world.features.ores.RadiumOreFeature;
import net.mcreator.minequest.world.features.ores.PlatinumOreFeature;
import net.mcreator.minequest.world.features.ores.PalladiumOreFeature;
import net.mcreator.minequest.world.features.ores.NickelOreFeature;
import net.mcreator.minequest.world.features.ores.ManganeseOreFeature;
import net.mcreator.minequest.world.features.ores.LithiumOreFeature;
import net.mcreator.minequest.world.features.ores.IridiumOreFeature;
import net.mcreator.minequest.world.features.ores.HafniumOreFeature;
import net.mcreator.minequest.world.features.ores.CobaltOreFeature;
import net.mcreator.minequest.world.features.ores.CadmiumOreFeature;

import java.util.Set;
import java.util.Map;
import java.util.HashMap;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class MinequestModFeatures {
	private static final Map<Feature<?>, FeatureRegistration> REGISTRY = new HashMap<>();
	static {
		REGISTRY.put(TitaniumOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES,
				TitaniumOreFeature.GENERATE_BIOMES, TitaniumOreFeature.CONFIGURED_FEATURE));
		REGISTRY.put(CobaltOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, CobaltOreFeature.GENERATE_BIOMES,
				CobaltOreFeature.CONFIGURED_FEATURE));
		REGISTRY.put(PlatinumOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES,
				PlatinumOreFeature.GENERATE_BIOMES, PlatinumOreFeature.CONFIGURED_FEATURE));
		REGISTRY.put(UraniumOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, UraniumOreFeature.GENERATE_BIOMES,
				UraniumOreFeature.CONFIGURED_FEATURE));
		REGISTRY.put(TungstenOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES,
				TungstenOreFeature.GENERATE_BIOMES, TungstenOreFeature.CONFIGURED_FEATURE));
		REGISTRY.put(LithiumOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, LithiumOreFeature.GENERATE_BIOMES,
				LithiumOreFeature.CONFIGURED_FEATURE));
		REGISTRY.put(RadiumOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, RadiumOreFeature.GENERATE_BIOMES,
				RadiumOreFeature.CONFIGURED_FEATURE));
		REGISTRY.put(NickelOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, NickelOreFeature.GENERATE_BIOMES,
				NickelOreFeature.CONFIGURED_FEATURE));
		REGISTRY.put(ZincOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, ZincOreFeature.GENERATE_BIOMES,
				ZincOreFeature.CONFIGURED_FEATURE));
		REGISTRY.put(ManganeseOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES,
				ManganeseOreFeature.GENERATE_BIOMES, ManganeseOreFeature.CONFIGURED_FEATURE));
		REGISTRY.put(VanadiumOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES,
				VanadiumOreFeature.GENERATE_BIOMES, VanadiumOreFeature.CONFIGURED_FEATURE));
		REGISTRY.put(SilverOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, SilverOreFeature.GENERATE_BIOMES,
				SilverOreFeature.CONFIGURED_FEATURE));
		REGISTRY.put(CadmiumOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, CadmiumOreFeature.GENERATE_BIOMES,
				CadmiumOreFeature.CONFIGURED_FEATURE));
		REGISTRY.put(IridiumOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, IridiumOreFeature.GENERATE_BIOMES,
				IridiumOreFeature.CONFIGURED_FEATURE));
		REGISTRY.put(HafniumOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, HafniumOreFeature.GENERATE_BIOMES,
				HafniumOreFeature.CONFIGURED_FEATURE));
		REGISTRY.put(TantalumOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES,
				TantalumOreFeature.GENERATE_BIOMES, TantalumOreFeature.CONFIGURED_FEATURE));
		REGISTRY.put(ZirconiumOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES,
				ZirconiumOreFeature.GENERATE_BIOMES, ZirconiumOreFeature.CONFIGURED_FEATURE));
		REGISTRY.put(PalladiumOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES,
				PalladiumOreFeature.GENERATE_BIOMES, PalladiumOreFeature.CONFIGURED_FEATURE));
		REGISTRY.put(YttriumOreFeature.FEATURE, new FeatureRegistration(GenerationStep.Decoration.UNDERGROUND_ORES, YttriumOreFeature.GENERATE_BIOMES,
				YttriumOreFeature.CONFIGURED_FEATURE));
	}

	@SubscribeEvent
	public static void registerFeature(RegistryEvent.Register<Feature<?>> event) {
		event.getRegistry().registerAll(REGISTRY.keySet().toArray(new Feature[0]));
		REGISTRY.forEach((feature, registration) -> Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, feature.getRegistryName(),
				registration.configuredFeature()));
	}

	@Mod.EventBusSubscriber
	private static class BiomeFeatureLoader {
		@SubscribeEvent
		public static void addFeatureToBiomes(BiomeLoadingEvent event) {
			for (FeatureRegistration registration : REGISTRY.values()) {
				if (registration.biomes() == null || registration.biomes().contains(event.getName())) {
					event.getGeneration().getFeatures(registration.stage()).add(() -> registration.configuredFeature());
				}
			}
		}
	}

	private static record FeatureRegistration(GenerationStep.Decoration stage, Set<ResourceLocation> biomes,
			ConfiguredFeature<?, ?> configuredFeature) {
	}
}
