
package net.mcreator.minequest.block;

import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.core.BlockPos;

import net.mcreator.minequest.init.MinequestModFluids;

public class MercuryBlock extends LiquidBlock {
	public MercuryBlock() {
		super(MinequestModFluids.MERCURY, BlockBehaviour.Properties.of(Material.WATER).strength(100f)

		);
		setRegistryName("mercury");
	}

	@Override
	public boolean propagatesSkylightDown(BlockState state, BlockGetter reader, BlockPos pos) {
		return true;
	}
}
