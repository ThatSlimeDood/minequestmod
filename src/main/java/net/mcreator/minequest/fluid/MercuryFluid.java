
package net.mcreator.minequest.fluid;

import net.minecraftforge.fluids.ForgeFlowingFluid;
import net.minecraftforge.fluids.FluidAttributes;

import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.resources.ResourceLocation;

import net.mcreator.minequest.init.MinequestModItems;
import net.mcreator.minequest.init.MinequestModFluids;
import net.mcreator.minequest.init.MinequestModBlocks;

public abstract class MercuryFluid extends ForgeFlowingFluid {
	public static final ForgeFlowingFluid.Properties PROPERTIES = new ForgeFlowingFluid.Properties(() -> MinequestModFluids.MERCURY,
			() -> MinequestModFluids.FLOWING_MERCURY,
			FluidAttributes.builder(new ResourceLocation("minequest:blocks/mercury_still"), new ResourceLocation("minequest:blocks/mercury_flow"))

					.density(500).viscosity(500)

	).explosionResistance(100f)

			.bucket(() -> MinequestModItems.MERCURY_BUCKET).block(() -> (LiquidBlock) MinequestModBlocks.MERCURY);

	private MercuryFluid() {
		super(PROPERTIES);
	}

	public static class Source extends MercuryFluid {
		public Source() {
			super();
			setRegistryName("mercury");
		}

		public int getAmount(FluidState state) {
			return 8;
		}

		public boolean isSource(FluidState state) {
			return true;
		}
	}

	public static class Flowing extends MercuryFluid {
		public Flowing() {
			super();
			setRegistryName("flowing_mercury");
		}

		protected void createFluidStateDefinition(StateDefinition.Builder<Fluid, FluidState> builder) {
			super.createFluidStateDefinition(builder);
			builder.add(LEVEL);
		}

		public int getAmount(FluidState state) {
			return state.getValue(LEVEL);
		}

		public boolean isSource(FluidState state) {
			return false;
		}
	}
}
